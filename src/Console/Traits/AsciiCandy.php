<?php

namespace FlorianRubel\Console\Traits;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Style\SymfonyStyle;

trait AsciiCandy
{
    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * Write a string in black.
     *
     * @param $str
     * @return string
     */
    protected function cBlack($str)
    {
        return '<fg=black>'.$str.'</>';
    }

    /**
     * Write a string in red.
     *
     * @param $str
     * @return string
     */
    protected function cRed($str)
    {
        return '<fg=red>'.$str.'</>';
    }

    /**
     * Write a string in green.
     *
     * @param $str
     * @return string
     */
    protected function cGreen($str)
    {
        return '<fg=green>'.$str.'</>';
    }

    /**
     * Write a string in yellow.
     *
     * @param $str
     * @return string
     */
    protected function cYellow($str)
    {
        return '<fg=yellow>'.$str.'</>';
    }

    /**
     * Write a string in blue.
     *
     * @param $str
     * @return string
     */
    protected function cBlue($str)
    {
        return '<fg=blue>'.$str.'</>';
    }

    /**
     * Write a string in magenta.
     *
     * @param $str
     * @return string
     */
    protected function cMagenta($str)
    {
        return '<fg=magenta>'.$str.'</>';
    }

    /**
     * Write a string in cyan.
     *
     * @param $str
     * @return string
     */
    protected function cCyan($str)
    {
        return '<fg=cyan>'.$str.'</>';
    }

    /**
     * Write a string in white.
     *
     * @param $str
     * @return string
     */
    protected function cWhite($str)
    {
        return '<fg=white>'.$str.'</>';
    }

    /**
     * More advanced and detailed progress bar.
     *
     * @param $count
     * @return ProgressBar
     */
    protected function getProgressBar($count)
    {
        $progress = new ProgressBar($this->io, $count);
        $progress->setEmptyBarCharacter($this->cWhite($this->getUTF8Char('2591')));
        $progress->setBarCharacter($this->cGreen($this->getUTF8Char('2588')));
        $progress->setMessage('');
        $progress->setProgressCharacter($this->cBlue($this->getUTF8Char('2588')));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% %message%');

        return $progress;
    }

    /**
     * Very simple minimalized progress visualization.
     *
     * @param int $current
     * @param int $max
     * @return string
     */
    protected function getInlineProgress($current, $max)
    {
        return $max.' / '.str_pad($current, strlen($max), '0', STR_PAD_LEFT).' # ';
    }

    /**
     * @see https://unicode-table.com/en/blocks/box-drawing/
     * @param $charCode
     * @return string
     */
    protected function getUTF8Char($charCode)
    {
        return html_entity_decode('&#x'.$charCode.';', ENT_NOQUOTES, 'UTF-8');
    }
}
